resource "aws_ecs_task_definition" "mysql" {
  family                   = "panaya-mysql"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn

  container_definitions = jsonencode([{
    name  = "mysql"
    image = var.mysql_docker_image
    portMappings = [{
      containerPort = 3306
      hostPort      = 3306
    }]
  }])
}

resource "aws_ecs_service" "mysql_service" {
  name            = "panaya-mysql-service"
  cluster         = aws_ecs_cluster.panaya_cluster.id
  task_definition = aws_ecs_task_definition.mysql.arn
  launch_type     = "FARGATE"
  network_configuration {
    subnets = [aws_subnet.private_subnet1.id, aws_subnet.private_subnet2.id]
  }
  desired_count   = 1
}
