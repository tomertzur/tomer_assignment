resource "aws_vpc" "vpc-panaya" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "vpc-panaya"
  }
}

resource "aws_subnet" "public_subnet1" {
  vpc_id     = aws_vpc.vpc-panaya.id
  cidr_block = var.public_subnet1_cidr
  map_public_ip_on_launch = true
  availability_zone = var.availability_zone1
}

resource "aws_subnet" "public_subnet2" {
  vpc_id     = aws_vpc.vpc-panaya.id
  cidr_block = var.public_subnet2_cidr
  map_public_ip_on_launch = true
  availability_zone = var.availability_zone2
}

resource "aws_subnet" "private_subnet1" {
  vpc_id     = aws_vpc.vpc-panaya.id
  cidr_block = var.private_subnet1_cidr
  availability_zone = var.availability_zone1
}

resource "aws_subnet" "private_subnet2" {
  vpc_id     = aws_vpc.vpc-panaya.id
  cidr_block = var.private_subnet2_cidr
  availability_zone = var.availability_zone2
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc-panaya.id
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public_subnet1.id
  depends_on    = [aws_internet_gateway.igw]
}

resource "aws_eip" "nat" {
  domain = "vpc"
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.vpc-panaya.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.vpc-panaya.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw.id
  }
}

resource "aws_route_table_association" "public_subnet1_association" {
  subnet_id      = aws_subnet.public_subnet1.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "public_subnet2_association" {
  subnet_id      = aws_subnet.public_subnet2.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private_subnet1_association" {
  subnet_id      = aws_subnet.private_subnet1.id
  route_table_id = aws_route_table.private_route_table.id
}

resource "aws_route_table_association" "private_subnet2_association" {
  subnet_id      = aws_subnet.private_subnet2.id
  route_table_id = aws_route_table.private_route_table.id
}
