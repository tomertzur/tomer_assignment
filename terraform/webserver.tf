resource "aws_ecs_task_definition" "webserver" {
  family                   = "panaya-webserver"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = aws_iam_role.ecs_execution_role.arn

  container_definitions = jsonencode([{
    name  = "webserver"
    image = var.webserver_docker_image
    portMappings = [{
      containerPort = 9980
      hostPort      = 9980
    }]
  }])
}

resource "aws_ecs_service" "webserver_service" {
  name            = "panaya-webserver-service"
  depends_on      = [aws_lb.webserver_lb]
  cluster         = aws_ecs_cluster.panaya_cluster.id
  task_definition = aws_ecs_task_definition.webserver.arn
  launch_type     = "FARGATE"
  network_configuration {
    subnets = [aws_subnet.private_subnet1.id, aws_subnet.private_subnet2.id]
    security_groups = [aws_security_group.webserver_sg.id]
  }
  desired_count   = 1
  load_balancer {
    target_group_arn = aws_lb_target_group.webserver_tg.arn
    container_name   = "webserver"
    container_port   = 9980
  }
}

resource "aws_lb" "webserver_lb" {
  name               = "panaya-webserver-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            =  [aws_subnet.public_subnet1.id, aws_subnet.public_subnet2.id]
}

resource "aws_lb_target_group" "webserver_tg" {
  name     = "webserver-tg"
  port     = 9980
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc-panaya.id
  target_type = "ip"
}

resource "aws_lb_listener" "webserver_listener" {
  load_balancer_arn = aws_lb.webserver_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.webserver_tg.arn
  }
}

resource "aws_security_group" "webserver_sg" {
  name        = "webserver-sg"
  description = "access to webserver"
  vpc_id      = aws_vpc.vpc-panaya.id

  ingress {
    from_port   = 9980
    to_port     = 9980
    protocol    = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg"
  description = "access to alb"
  vpc_id      = aws_vpc.vpc-panaya.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
