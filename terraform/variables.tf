variable "vpc_cidr" {
  description = "CIDR VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet1_cidr" {
  description = "CIDR public subnet1"
  default     = "10.0.1.0/24"
}

variable "public_subnet2_cidr" {
  description = "CIDR public subnet2"
  default     = "10.0.4.0/24"
}


variable "private_subnet1_cidr" {
  description = "CIDR private subnet1"
  default     = "10.0.2.0/24"
}

variable "private_subnet2_cidr" {
  description = "CIDR private subnet2"
  default     = "10.0.3.0/24"
}

variable "availability_zone1" {
  description = "AZ"
  default     = "eu-central-1a"
}

variable "availability_zone2" {
  description = "AZ"
  default     = "eu-central-1b"
}

variable "mysql_docker_image" {
  description = "Image for mysql"
  default     = "tomer30tzur/panaya-home-assignment:panaya-mysql-image"
}

variable "webserver_docker_image" {
  description = "Image for webserver"
  default     = "tomer30tzur/panaya-home-assignment:panaya-webserver-image"
}

variable "aws_region" {
  description = "Region"
  default     = "eu-central-1"
}
