#!/bin/bash

basepath=`dirname $(readlink -f $0)`
cd ${basepath}
export LOGFILE_relative_path="../logs/redeploy_webserver.log"
echo "" > ${LOGFILE_relative_path}
export LOGFILE=$(realpath ${LOGFILE_relative_path})
source functions.sh

log "Going to redeploy webserver only."
# docker build within specific network doesn't work with the new Docker BuildKit
export DOCKER_BUILDKIT=0
remove_webserver
cd ${basepath}
build_run_webserver
