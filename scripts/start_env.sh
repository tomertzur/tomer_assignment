#!/bin/bash

basepath=`dirname $(readlink -f $0)`
cd ${basepath}
mkdir -p ../logs
export LOGFILE_relative_path="../logs/start_env.log"
echo "" > ${LOGFILE_relative_path}
export LOGFILE=$(realpath ${LOGFILE_relative_path})
source functions.sh

log "Going to start the environment"
# docker build within specific network doesn't work with the new Docker BuildKit
export DOCKER_BUILDKIT=0
build_run_mysql
cd ${basepath}
build_run_webserver
