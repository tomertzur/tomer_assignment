# tomer_assignment


## Prerequisites

- [ ] [Linux] Please run it only on Linux/MAC.
- [ ] [Docker] Please install Docker before running the scripts.
- [ ] [TF] Please install TF before running the terraform init/apply.
    - Also, export AWS_ACCESS_KEY_ID and export AWS_SECRET_ACCESS_KEY before running TF.

## Usage
```
git clone https://gitlab.com/tomertzur/tomer_assignment.git
cd tomer_assignment/scripts
```
- [Start] In order to start the environment, run start_env.sh. 
    - For your convenience you can add data to the mysql by running (in example):
    ```
    docker exec -it panaya-mysql-container bash
    insert_mysql_data.sh --company-id 10 --company-name NewCompany --account-id 10 --account-name NewAccount --project-id 10 --project-name NewProject --project-status 0
    ```
- [Redeploy] For redeploying the webserver, run redeploy_webserver.sh. 
- [Stop] Stopping/destroying the environments can be done via stop_env.sh.
- [Logs] for your convenience, if you want to see more logs, look at logs directory that being created during the start/redeploy/stop scripts.
- [TF] 
    - cd to terraform directory.
    - run terraform init. 
    - terraform apply -auto-approve 
    - Region - eu-central-1 (Frankfurt) region.
    - ALB DNS will be displayed when terraform apply is done. 
    - Please wait 5-10 minutes before trying to reach the ALB DNS.

## CI/CD
Possible CI/CD for this project is -

- [GIT] 
    - Remove sensitive data (username, password) from files.
    - Store sensitive data in Credentials in Jenkins.
    - Everyone who works on this repository will create a FB from development branch.
    - Once a week, merging development branch to main.
    - Every MR will trigger (webhook) a job in:
- [Jenkins]
    - CI job:
        - Merge the target branch to the FB.
        - withCredentials in Jenkins - add username and password to Dockerfile. 
        - Starting the env by start_env.sh.
        - Insert some data to the mysql.
        - Redeploy webserver - verify the new data exists.
        - Remove username and password from Dockerfile. 
        - Only if everything works as expected - Merge button will be available.
    - CD job:
        - Trigger CD job after merge button.
        - add username and password to Dockerfile. 
        - Build and start the env via start_env.sh
        - Push the images to docker.snapshot repository in Nexus in case it's a deployment branch, in case it's main - to docker.release.
        - Push images to the ECR for AWS deployment.
        - Change images version in TF.
        - Run TF in Dev env - if it passes, run it in production.