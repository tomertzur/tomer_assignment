CREATE DATABASE IF NOT EXISTS panaya_db;
USE panaya_db;

CREATE TABLE As_company (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL
);

CREATE TABLE As_account (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Company_id INT,
    FOREIGN KEY (Company_id) REFERENCES As_company(Id)
);

CREATE TABLE As_project (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(255) NOT NULL,
    Account_id INT,
    Status INT CHECK (Status IN (0, 1, 2)),
    FOREIGN KEY (Account_id) REFERENCES As_account(Id)
);

