import pymysql
import os
import pandas as pd

DB_HOST="panaya-mysql-container"

def get_db_connection():
    return pymysql.connect(host=DB_HOST, user="root", passwd=os.environ['MYSQL_ROOT_PASSWORD'], db=os.environ['MYSQL_DATABASE'])

def query_from_db(db_connection):
    query = """
    SELECT c.Id AS 'Company ID', c.Name AS 'Company Name', 
           a.Id AS 'Account ID', a.Name AS 'Account Name', 
           p.Id AS 'Project ID', p.Name AS 'Project Name', 
           p.Status AS 'Project Status'
    FROM As_company c
    JOIN As_account a ON c.Id = a.Company_id
    JOIN As_project p ON a.Id = p.Account_id
    """
    return pd.read_sql(query, db_connection)

def replace_status_with_text(dataframe):
    status_translator = {0: 'Inactive', 1: 'Active', 2: 'Frozen'}
    dataframe['Project Status'] = dataframe['Project Status'].map(status_translator)
    return dataframe

def main():
    db_connection = get_db_connection()
    
    db_dataframe = query_from_db(db_connection)

    db_connection.close()
    
    db_dataframe = replace_status_with_text(db_dataframe)
    
    html_output = db_dataframe.to_html(index=False)
    
    with open("/data/index.html", "w") as f:
        f.write(html_output)

main()
